package com.adsi.bike.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private long id;

    @NotNull
    @Size(max=20, min=5)
    @Column(name = "user_name", length = 20)
    private String userName;

    @NotNull
    @Size(max=20, min=5)
    @Column(name = "password", length = 20)
    private String password;

    //relation with detailUser

    @JoinColumn(name = "id_detail_user", unique = true)
    @OneToOne(cascade = CascadeType.ALL) //uno a uno pero si hago modificacion en user automaticamente//hacer modificacion en DetailUser
    private DetailUser detailUser;

    @NotEmpty(message = "Email no puede ser vacio")
    @Email(message = "El email no tiene la estructura correcta")
    @Column(unique = true)
    private String email;

    private Boolean enabled;

    //relation With rols
    @ManyToMany
    @JoinTable(name = "user_has_rol",
    joinColumns = @JoinColumn(name = "id_user"),
    inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private List<Rols> rols;
}
