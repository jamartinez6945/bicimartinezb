package com.adsi.bike.repository;

import com.adsi.bike.domain.DetailUser;
import com.adsi.bike.domain.Users;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository  extends CrudRepository<Users, Long> {

    Users findByDetailUser (DetailUser detailUser);

    Users findByUserName(String username);
}
