package com.adsi.bike.repository;

import com.adsi.bike.domain.Bike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BikeRepository extends JpaRepository<Bike, Integer> {

    Optional<Bike> findBySerial(String serial);

    Iterable<Bike> findBySerialContains(String serial);

    Iterable<Bike> findByModelContains(String model);

    Iterable<Bike> findBySerialContainsAndModelContains(String serial, String model);


    //find quantity of bikes
    @Query(value = "select count (bike) from Bike bike")
    Integer quantityBikes();

    //Inventory Value
    @Query(value = "select sum(bike.price) from Bike bike")
    Double inventoryValue();

    @Query(value = "select bike.model, bike.price from Bike bike where bike.model like %:model%")
    Iterable<Bike> findByModelQuery(@Param("model") String model);


}
