package com.adsi.bike.auth;

public class JwtConfig {

    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEA3tyWSRE52i7CCJPF6UBrR9OXugBnuf2tbkEWxJ8aWraka9LZ\n" +
            "iDVd1LMv0hArMtk6ODjdpBNFOe3BR7wxyPEdxHU/thpeeXnFj4CFyNwNt27EhXMx\n" +
            "DmJ+dpyZNlBavE3JjV12FnI8qypOm2ShEyYEthkq8VtvSVZ/mmLY6gDfjfqJQgLj\n" +
            "+wohE0obO2RKYoAeL7BtDK27kqTWWWUaxXG2ONwjgW9SVh6f683h2gymYCUn+X3U\n" +
            "Yd0RiYWjGd17bBpS5PcSq346cLkGnFyv47eDebjC2U5nNXqbULFjowfoY0lvFL5r\n" +
            "Gquja7WYvsQVaZe9J/jhrVnaJFK6+rCZZXNhOQIDAQABAoIBAHKQfwlMu6q3grOa\n" +
            "E5FWtk+ULFlHwArSp4cYGrY0+/1KCrgLChyFIpknDBw2h556IP2p7fZcmaev2z4f\n" +
            "NpCPsre9dmUMpShZOwAMF7ortTlozcm0b7FvXIdo9TIAmMRYUJZFR2E+lIWBGCu1\n" +
            "zT/kuWNLCNJOKKVQvAQHcG7KyCFZhaje6rCdJVWm5pdXKAV0djrD7Sf53YVaWB9L\n" +
            "pVsAo86ZXtRiSZJPLQHj0G7l9IzkDdPZ2g42UBornybmxDx7Bkc5I+SaD6zYohkQ\n" +
            "zWdsL3z8zvQYlPEPzCx60kEmtO5W3LnlWSgFn5EjqpkuwFB2pvL4WUNrqHbqFMOc\n" +
            "1lWQYQECgYEA/Da4s91gYaf8Dbh0Wg1uxCxGphbV9oiaeWbzEFNPUPv8A0us3oeF\n" +
            "Zd7JnKLnw3AvoWRtNJe4PC05mBAfsur1+QvfpIvtxgcQX4W7QAol/eoRQxdXFu24\n" +
            "laU12EiD85iReQBU6a7/ltrPB/Gp1OtdNmreRi0LbPr3znKcsn4r+xECgYEA4jUQ\n" +
            "IyQP+x+mh7Y6+SB/EF9m3CGK82HKjaCEozCabvhLwhBlWn+0BmMHFFXPORW8Nf3D\n" +
            "HkMFZyFVUZSR8f/Bziq8fuc2dCXBKK3G6PyOUscbeIRQVP6EHSHsaOa8BM1NwPlr\n" +
            "xvQT8QmqOAL3j2OBF55GFdtsQn+FLNWC7zqCc6kCgYAv/1n3INIOw6E8GzZJTPUf\n" +
            "cC2hvgbJ/AUpxWXMXY5ioxG6rtQfJTtFv7gujvH8R4VI+YuuzpDJIvIq9iaSLhTR\n" +
            "aGkPL3H4la+P/is8bLH+OGVAl1iDjInsMJympGY4HXjRxjZiomMRmmJ76Imhc4kx\n" +
            "qfTS9g+OldRC/ZzEey5zgQKBgDZwRr3uQ9pPt6gZdv/Vjj7Gdt6rQ+Fmu3RyGPmd\n" +
            "DM1/+9tsTrgE27JdyqpeLEy0qmLj+z0hK/+xyfKkKqtuvZl8N3ji8/vnyiCcEa6o\n" +
            "eSf6uuW6EgsiZXENZPOp873EGt5CKGns+4+an9hy/lkHvV2NvZgtAlTOk6JCkAw6\n" +
            "rIJhAoGBAMjkktMmqpa1noNJIr39Lqm1kVkFze+oAGHB+AKeJPWb9iQtKK2+9+K1\n" +
            "sFfrbNzivg0QeyysCYXjOHvMKZAXeA1rm1FdivvdPUtyhM87BHQE/UZkebMfdNaL\n" +
            "XU+MKnxmzJLplvdz2PJ7Kt7icRZVzDpn15ipbX8bBhW63wZ1FDbP\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3tyWSRE52i7CCJPF6UBr\n" +
            "R9OXugBnuf2tbkEWxJ8aWraka9LZiDVd1LMv0hArMtk6ODjdpBNFOe3BR7wxyPEd\n" +
            "xHU/thpeeXnFj4CFyNwNt27EhXMxDmJ+dpyZNlBavE3JjV12FnI8qypOm2ShEyYE\n" +
            "thkq8VtvSVZ/mmLY6gDfjfqJQgLj+wohE0obO2RKYoAeL7BtDK27kqTWWWUaxXG2\n" +
            "ONwjgW9SVh6f683h2gymYCUn+X3UYd0RiYWjGd17bBpS5PcSq346cLkGnFyv47eD\n" +
            "ebjC2U5nNXqbULFjowfoY0lvFL5rGquja7WYvsQVaZe9J/jhrVnaJFK6+rCZZXNh\n" +
            "OQIDAQAB\n" +
            "-----END PUBLIC KEY-----\n";
}
