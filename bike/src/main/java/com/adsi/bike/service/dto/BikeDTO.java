package com.adsi.bike.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class BikeDTO {

    @NotNull
    private int id;

    @NotEmpty(message = "este campo no puede estar vacio")
    @Size(min = 2, max = 10, message = "El tamaño del campo debe ser entre 2 y 10 caracteres")
    private String model;

    @NotEmpty(message = "este campo no puede estar vacio")
    @Size(min = 2, max = 10, message = "El tamaño del campo debe ser entre 2 y 10 caracteres")
    private String serial;

    private double price;


}
