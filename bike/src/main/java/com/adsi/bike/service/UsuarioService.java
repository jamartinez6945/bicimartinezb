package com.adsi.bike.service;

import com.adsi.bike.domain.Users;
import com.adsi.bike.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users usuario = userRepository.findByUserName(username);

        if (username == null){
            logger.error("Error en el login: No existe el usuario" + username + " ");
            throw new UsernameNotFoundException("Error en el login: No existe el usuario" + username + " ");
        }

        List<GrantedAuthority> authorities = usuario.getRols()
                .stream()
                .map(rols -> new SimpleGrantedAuthority(rols.getDescription()))
                .peek(authority -> logger.info("Role: " +authority.getAuthority()))
                .collect(Collectors.toList());
        return new User(usuario.getUserName(),usuario.getPassword(),usuario.getEnabled(), true, true, true,authorities);

    }
}
